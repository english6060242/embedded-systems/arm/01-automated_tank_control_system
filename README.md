# Automated Tank Control System

This repository contains the source code for an Automated Tank Control System developed using an LPC microcontroller with CMSIS libraries in MCU Expresso.
Board: LCP1769

## Description

The Automated Tank Control System is designed to efficiently manage liquid levels in a tank through various input modes. It offers a user-friendly interface for configuration and monitoring, utilizing UART communication for user interaction.

![Diagram](img/diagram.png)

## Components and Functionalities

### Sensors
- Utilizes the ADC to measure the potentiometer's voltage for Potentiometer-based Control (P0.23).
- Monitors external interrupt pins for tank filling (P0.7), stopping (P0.8), or emptying (P0.9).

### Actuators
- Controls the pump or valve system for liquid flow into/out of the tank.
- Manages LED indicators for system status indication and error signaling.
  - P0.0 LED turns on in mode 0.
  - P0.1 LED turns on in mode 1.
  - P0.18 LED turns on in mode 2.
  - P0.17 LED turns on in mode 3.
  - P0.15 LED turns on with UART error.

### Microcontroller and Peripherals
- Utilizes an LPC microcontroller, configuring GPIO pins and timer peripherals (TMR0) for specific tasks.
- Interfaces with UART for communication with external devices for user input, configuration, and status display.

### Control Modes
- Supports multiple operating modes:
  - 0: Manual Mode: Allows manual control of tank filling and emptying.
  - 1: Control via PC: Enables setting the tank level (%) via UART commands from a connected PC.
  - 2: Potentiometer-based Control: Possibly enables control using a potentiometer to set the desired tank water level (%).
  - 3: Cleaning Mode: A dedicated mode for tank cleaning operations.

### Control Logic
- The control algorithm continuously monitors the tank level sensor, dictating pump operations based on selected modes and predefined setpoints.
- Automatic control modes adjust pump operation to maintain the desired tank level set by the PC or other methods.

### User Interaction
- Provides a menu-driven interface through UART for users to select operational modes, set tank levels, and enable/disable automated control.

### Error Handling and Monitoring
- Implements error handling for UART communication errors using LED indicators and system flags.
- Monitors UART communication line status for potential errors and incorporates mechanisms to signal and handle these errors.

## License

This project is licensed under the [MIT License](LICENSE.md).
