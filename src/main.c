#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include "lpc17xx_uart.h"
#include "lpc17xx_pinsel.h"
#include <lpc17xx_gpio.h>
#include <lpc17xx_nvic.h>
#include "lpc17xx_exti.h"
#include <lpc17xx_timer.h>
#include "lpc17xx_adc.h"
#include "stdlib.h"

// Definitions
#define VREF       3.3 // Reference voltage at pin VREFP, given VREFN = 0V (GND)
#define ADC_CLK_EN (1<<12)
#define SEL_AD0_0  (1<<0) // Select ADC channel AD0.0
#define CLKDIV     1 // ADC clock divider (ADC_CLOCK = PCLK / CLKDIV + 1) = 12.5MHz @ 25MHz PCLK
#define PWRUP      (1<<21) // Set to 0 => power down
#define START_CNV  (1<<24) // 001 to start conversion immediately
#define ADC_DONE   (1U<<31) // Define it as an unsigned value or the compiler will throw a #61-D warning
#define ADCR_SETUP_SCM ((CLKDIV<<8) | PWRUP) // ADC setup for conversion

// Configuration Functions
void configurePins(void);
void configureUART(void);
void configureTimer0(void);
void configureADC(void);

// Interrupt Service Routine Functions
void UARTInterruptRoutine(void);
void TimerInterruptRoutine(void);

// GPIO Command Functions
void DisplayMode(void);
void Pump(void);

// Functions for Printing Messages
void GeneralMenu(void);
void ModeMenu(void);
void RequestDataMode1(void);
void ModeError(void);
void ControlError(void);
void Clear(void);

// Global Variables
uint8_t mode = 0;
uint8_t TankLevel = 50;
uint8_t SetLevelPC = 0;
uint8_t SetLevelADC = 0;
uint8_t PumpState = 2;
uint8_t ControlEnabled = 0; // 0 -> Disabled; 1 -> Enabled

uint8_t receiveBuffer1[4];

int a = 0;

void EINT3_IRQHandler(void)
{
    NVIC_ClearPendingIRQ(EINT3_IRQn);
    if(GPIO_GetIntStatus(0,9,0)==ENABLE)
    {
        if(mode == 0)
            PumpState = 1;  // Emptying the tank
        GPIO_ClearInt(0,(1<<9));    // Lower flag for P0.9
    }
    if(GPIO_GetIntStatus(0,8,0)==ENABLE)
    {
        if(mode == 0)
            PumpState = 2;  // Stopped
        GPIO_ClearInt(0,(1<<8));    // Lower flag for P0.8
    }
    if(GPIO_GetIntStatus(0,7,0)==ENABLE)
    {
        if(mode == 0)
            PumpState = 3;  // Filling the tank
        GPIO_ClearInt(0,(1<<7));    // Lower flag for P0.7
    }
    EXTI_ClearEXTIFlag(EXTI_EINT3); // Lower external interrupt 3 flag
}

void UART0_IRQHandler(void)
{
	SetNivelADC /= 2.55;
	RutinaIntUART();
	NVIC_ClearPendingIRQ(UART0_IRQn);
}

void TIMER0_IRQHandler()
{
	SetNivelADC /= 2.55;
	a = ~a;
	RutinaIntTMR();
	NVIC_ClearPendingIRQ(TIMER0_IRQn);
	LPC_TIM0->IR |= (1<<0);
	TIM_ClearIntPending(LPC_TIM0, TIM_MR0_INT);
}


int main(void)
{
    uint16_t value = 0;

    // Configure
    pinConfig();
    uartConfig();
    TMR0_Config();
    ADC_Config();

    // Print initial message
    GeneralMenu();

    while(1)
    {
        DisplayMode();
        Pump();
        LPC_ADC->ADCR |= START_CNV; // Start new conversion
        while((LPC_ADC->ADDR0 & ADC_DONE) == 0); // Wait until conversion is finished
        value = ADC_GetData(0); // Get data from channel 0
        SetLevelADC = (uint8_t)(value >> 4);
    }
    return 0;
}

void Clean(void)
{
    uint8_t info0[1] = {27}; // ASCII code for clearing the display

    UART_Send(LPC_UART0, info0, sizeof(info0), BLOCKING);

    return;
}

void GeneralMenu(void)
{
    uint8_t info0[] = "\nCurrent Mode: ";
    uint8_t info1[1] = {mode + 48}; // Converts mode to ASCII value
    uint8_t info2[] = "\nTank level set by PC: "; // For some reason, when it's a single digit, it adds a character
    uint8_t info3[3];
    uint8_t info4[] = "\nCurrent tank level: ";
    uint8_t info5[3];
    uint8_t info6[] = "\nTank level set by ADC: ";
    uint8_t info7[3];
    uint8_t info8[] = "\nEnter the appropriate value to select from the following options\n"
            "    0: Disable Auto-Control\n    1: Enable Auto-Control\n    2: Change level"
            " set by PC\n    3: Change operating mode\n";
    uint8_t Hab[] = "\nAuto-Control Enabled";
    uint8_t Deshab[] = "\nAuto-Control Disabled";

    itoa(SetLevelPC, info3, 10);    // Convert "SetLevel" value to ASCII in decimal system
    itoa(TankLevel, info5, 10); // Same for TankLevel
    itoa(SetLevelADC, info7, 10);   // Same for SetLevelADC

    if (ControlEnabled)
        UART_Send(LPC_UART0, Hab, sizeof(Hab), BLOCKING);
    else
        UART_Send(LPC_UART0, Deshab, sizeof(Deshab), BLOCKING);
    UART_Send(LPC_UART0, info0, sizeof(info0), BLOCKING);
    UART_Send(LPC_UART0, info1, sizeof(info1), BLOCKING);
    UART_Send(LPC_UART0, info2, sizeof(info2), BLOCKING);
    UART_Send(LPC_UART0, info3, sizeof(info3), BLOCKING);
    UART_Send(LPC_UART0, info4, sizeof(info4), BLOCKING);
    UART_Send(LPC_UART0, info5, sizeof(info5), BLOCKING);
    UART_Send(LPC_UART0, info6, sizeof(info6), BLOCKING);
    UART_Send(LPC_UART0, info7, sizeof(info7), BLOCKING);
    UART_Send(LPC_UART0, info8, sizeof(info8), BLOCKING);

    return;
}

void ModeMenu(void)
{
    uint8_t info[] = "\nSelect the mode in which you want to operate:\n    0: Manual Mode\n"
            "    1: PC Level Control\n    2: Pot Level Control\n"
            "    3: Cleaning Mode\n";

    UART_Send(LPC_UART0, info, sizeof(info), BLOCKING);

    return;
}

void RequestDataMode1(void)
{
    uint8_t info[] = "\nMode 1 selected. Enter the level at which you want to maintain the tank"
            "\n0000 -> 0% \n0100 -> 100% \n";

    UART_Send(LPC_UART0, info, sizeof(info), BLOCKING);
    UART_Send(LPC_UART0, "\n", sizeof("\n"), BLOCKING);

    return;
}

void ModeError(void)
{
    uint8_t info[] = "\nError, that mode is already selected. Enter a different mode than the current one\n";

    UART_Send(LPC_UART0, info, sizeof(info), BLOCKING);

    return;
}

void ErrorControl(void)
{
    uint8_t info[] = "\nError: Auto-Control must be disabled to execute that command\n";

    UART_Send(LPC_UART0, info, sizeof(info), BLOCKING);

    return;
}

void ShowMode(void)
{
    switch(mode)
    {
        case 0:
            GPIO_SetValue(0, (1 << 0));
            GPIO_ClearValue(0, (1 << 1));
            GPIO_ClearValue(0, (1 << 18));
            GPIO_ClearValue(0, (1 << 17));
            break;
        case 1:
            GPIO_ClearValue(0, (1 << 0));
            GPIO_SetValue(0, (1 << 1));
            GPIO_ClearValue(0, (1 << 18));
            GPIO_ClearValue(0, (1 << 17));
            break;
        case 2:
            GPIO_ClearValue(0, (1 << 0));
            GPIO_ClearValue(0, (1 << 1));
            GPIO_SetValue(0, (1 << 18));
            GPIO_ClearValue(0, (1 << 17));
            break;
        case 3:
            GPIO_ClearValue(0, (1 << 0));
            GPIO_ClearValue(0, (1 << 1));
            GPIO_ClearValue(0, (1 << 18));
            GPIO_SetValue(0, (1 << 17));
            break;
        default:
            GPIO_ClearValue(0, (1 << 0));
            GPIO_ClearValue(0, (1 << 1));
            GPIO_ClearValue(0, (1 << 18));
            GPIO_ClearValue(0, (1 << 17));
    }
    return;
}

void Pump(void)
{
	switch(PumpState)
		{
			case 1:
				GPIO_SetValue(2,(1<<0));
				GPIO_ClearValue(2,(1<<1));
				GPIO_ClearValue(2,(1<<2));
				break;
			case 2:
				GPIO_ClearValue(2,(1<<0));
				GPIO_SetValue(2,(1<<1));
				GPIO_ClearValue(2,(1<<2));
				break;
			case 3:
				GPIO_ClearValue(2,(1<<0));
				GPIO_ClearValue(2,(1<<1));
				GPIO_SetValue(2,(1<<2));
				break;

			default:
				GPIO_ClearValue(2,(1<<0));
				GPIO_SetValue(2,(1<<1));
				GPIO_ClearValue(2,(1<<2));
		}
		return;
}

void UARTInterruptRoutine(void)
{
    static uint8_t opc_sel = 5;  // Selected option from the main menu
    uint8_t receive_buffer[1];
    uint32_t interrupt_source, temp, temp1;
    UART_FIFO_CFG_Type UARTFIFO_cfg;

    // Check the interrupt source
    interrupt_source = UART_GetIntId(LPC_UART0);
    temp = interrupt_source & UART_IIR_INTID_MASK;

    // If data is received
    if ((temp == UART_IIR_INTID_RDA) || (temp == UART_IIR_INTID_CTI))
    {
        if (opc_sel == 5)
        {
            UART_Receive(LPC_UART0, receive_buffer, sizeof(receive_buffer), NONE_BLOCKING);
            opc_sel = atoi(receive_buffer);
            if (opc_sel == 0)  // Disable Pump
            {
                ControlEnabled = 0;
                PumpState = 2;
                opc_sel = 5;
                GeneralMenu();
            }
            if (opc_sel == 1)  // Enable Pump
            {
                ControlEnabled = 1;
                opc_sel = 5;
                GeneralMenu();
            }
            if (opc_sel == 2)  // Change Level Set by PC
            {
                if (!ControlEnabled)
                {
                    RequestDataMode1();
                    UART_FIFOConfigStructInit(&UARTFIFO_cfg);
                    UARTFIFO_cfg.FIFO_Level = UART_FIFO_TRGLEV1;
                    UART_FIFOConfig(LPC_UART0, &UARTFIFO_cfg);
                    // Now 4 characters will be received
                }
                else
                {
                    ErrorHab();
                    opc_sel = 5;
                    GeneralMenu();
                }
            }
            if (opc_sel == 3)  // Select Mode
            {
                if (!ControlEnabled)
                    ModeMenu();
                else
                {
                    ErrorHab();
                    opc_sel = 5;
                    GeneralMenu();
                }
            }
        }
        else
        {
            if (opc_sel == 2) // Data to set the level
            {
                UART_Receive(LPC_UART0, receive_buffer, sizeof(receive_buffer), NONE_BLOCKING);
                SetLevelPC = atoi(receive_buffer);
                UART_FIFOConfigStructInit(&UARTFIFO_cfg);
                UARTFIFO_cfg.FIFO_Level = UART_FIFO_TRGLEV0;
                UART_FIFOConfig(LPC_UART0, &UARTFIFO_cfg);
                // Now a single character will be received
                opc_sel = 5;
                GeneralMenu();
            }
            if (opc_sel == 3) // Data to select mode
            {
                UART_Receive(LPC_UART0, receive_buffer, sizeof(receive_buffer), NONE_BLOCKING);
                if (atoi(receive_buffer) != mode)
                {
                    mode = atoi(receive_buffer);
                }
                else
                {
                    ErrorMode();
                }
                opc_sel = 5;
                GeneralMenu();
            }
        }
    }

    // If there was any error
    if (temp == UART_IIR_INTID_RLS)
    {
        temp1 = UART_GetLineStatus(LPC_UART0);
        temp1 &= (UART_LSR_OE | UART_LSR_PE | UART_LSR_FE | UART_LSR_BI | UART_LSR_RXFE);
        if (temp1)  // If any of those errors occurred, infinite loop. Here you could try to fix the error
        {
            while (1)
            {
                GPIO_SetValue(0, (1 << 15));
            }  // Turn on P0.15 indicating an error and requesting a reset
        }
    }
    return;
}

void TimerInterruptRoutine(void)
{
    if (mode == 0)
    {
        if (PumpState == 1)
        {
            if (TankLevel == 0)
            {
                PumpState = 2;
            }
            else
            {
                TankLevel--;
                GeneralMenu();
            }
        }
        if (PumpState == 3)
        {
            if (TankLevel == 100)
            {
                PumpState = 2;
            }
            else
            {
                TankLevel++;
                GeneralMenu();
            }
        }
    }
    if (ControlEnabled)
    {
        if (mode == 1)
        {
            if (TankLevel > SetLevelPC)
            {
                PumpState = 1;
                TankLevel--;
                GeneralMenu();
            }
            if (TankLevel < SetLevelPC)
            {
                PumpState = 3;
                TankLevel++;
                GeneralMenu();
            }
            if (TankLevel == SetLevelPC)
            {
                PumpState = 2;
                mode = 0;
                ControlEnabled = 0;
                GeneralMenu();
            }
        }
        if (mode == 2)
        {
            if (TankLevel > SetLevelADC)
            {
                PumpState = 1;
                TankLevel--;
                GeneralMenu();
            }
            if (TankLevel < SetLevelADC)
            {
                PumpState = 3;
                TankLevel++;
                GeneralMenu();
            }
            if (TankLevel == SetLevelADC)
            {
                PumpState = 2;
                mode = 0;
                ControlEnabled = 0;
                GeneralMenu();
            }
        }
        if (mode == 3)
        {
            FIO_SetMask(0, 0x00000380, 0); // Mask pins P0.7, P0.8, and P0.9
            if (TankLevel == 0)
            {
                PumpState = 2;
                mode = 0;
                ControlEnabled = 0;
                FIO_SetMask(0, 0x00000380, 1); // Unmask pins P0.7, P0.8, and P0.9
            }
            else
            {
                PumpState = 1;
                TankLevel--;
            }
            GeneralMenu();
        }
    }
    return;
}

void pinConfig(void)
{
    PINSEL_CFG_Type pinCFG;

    // Configure P0.2 as UART0 Tx
    pinCFG.Portnum = 0;
    pinCFG.Pinnum = 2;
    pinCFG.Funcnum = 1;
    pinCFG.Pinmode = 0;
    pinCFG.OpenDrain = 0;
    PINSEL_ConfigPin(&pinCFG);
    GPIO_SetDir(0, (1 << 2), 1); // P0.2 is an output

    // Configure P0.3 as UART0 Rx
    pinCFG.Pinnum = 3;
    PINSEL_ConfigPin(&pinCFG);
    GPIO_SetDir(0, (1 << 3), 0); // P0.3 is an input

    // Configure P0.0, P0.1, P0.18, P0.17, and P0.15 as GPIO
    pinCFG.Pinmode = 0;
    pinCFG.Funcnum = 0;

    pinCFG.Pinnum = 0;
    PINSEL_ConfigPin(&pinCFG);

    pinCFG.Pinnum = 1;
    PINSEL_ConfigPin(&pinCFG);

    pinCFG.Pinnum = 18;
    PINSEL_ConfigPin(&pinCFG);

    pinCFG.Pinnum = 17;
    PINSEL_ConfigPin(&pinCFG);

    pinCFG.Pinnum = 15;
    PINSEL_ConfigPin(&pinCFG);

    // Pins for external interrupts
    pinCFG.Pinmode = PINSEL_PINMODE_PULLDOWN;

    pinCFG.Pinnum = 7;
    PINSEL_ConfigPin(&pinCFG);

    pinCFG.Pinnum = 8;
    PINSEL_ConfigPin(&pinCFG);

    pinCFG.Pinnum = 9;
    PINSEL_ConfigPin(&pinCFG);

    // Configure P0.0, P0.1, P0.18, P0.17, and P0.15 as outputs
    GPIO_SetDir(0, 0x00068003, 1);

    // Configure P0.9, P0.8, and P0.7 as inputs for interrupts
    GPIO_SetDir(0, 0x00000380, 0);
    GPIO_IntCmd(0, 0x00000380, 0); // 0-> Rising Edge
    NVIC_EnableIRQ(EINT3_IRQn);

    // Configure P2.0, P2.1, P2.2 as GPIO outputs
    pinCFG.Portnum = 2;

    pinCFG.Pinnum = 0;
    PINSEL_ConfigPin(&pinCFG);

    pinCFG.Pinnum = 1;
    PINSEL_ConfigPin(&pinCFG);

    pinCFG.Pinnum = 2;
    PINSEL_ConfigPin(&pinCFG);

    GPIO_SetDir(2, 0x00000007, 1); // Configure P2.0, P2.1, P2.2 as outputs

    // ADC pin configuration
    pinCFG.Portnum = 0; 
    pinCFG.Pinnum = 23;
    pinCFG.Funcnum = 1;
    PINSEL_ConfigPin(&pinCFG); // Configure P0.23 for ADC
    GPIO_SetDir(0, (1 << 23), 0); // P0.23 is an input

    return;
}

void TMR0_Config()
{
    TIM_TIMERCFG_Type tmr;
    TIM_MATCHCFG_Type mtch;

    tmr.PrescaleOption = TIM_PRESCALE_USVAL;
    tmr.PrescaleValue = 100;

    mtch.MatchChannel = 0;
    mtch.IntOnMatch = ENABLE;
    mtch.StopOnMatch = DISABLE;
    mtch.ResetOnMatch = ENABLE;
    mtch.ExtMatchOutputType = TIM_EXTMATCH_NOTHING;
    mtch.MatchValue = 10000;

    TIM_Init(LPC_TIM0, TIM_TIMER_MODE, &tmr);
    TIM_ConfigMatch(LPC_TIM0, &mtch);

    TIM_Cmd(LPC_TIM0, ENABLE);
    NVIC_EnableIRQ(TIMER0_IRQn);

    return;
}

void ADC_Config(void)
{
    LPC_SC->PCONP |= ADC_CLK_EN; // Enable ADC clock
    LPC_ADC->ADCR = ADCR_SETUP_SCM | SEL_AD0_0;
    LPC_PINCON->PINSEL1 |= (1 << 14); // Select AD0.0 for P0.23

    return;
}

void uartConfig(void)
{
    UART_CFG_Type UART_cfg;
    UART_FIFO_CFG_Type UARTFIFO_cfg;

    // UART_cfg.Baud_rate = 9600;
    // UART_cfg.Databits = UART_DATABIT_8;
    // UART_cfg.Parity = UART_Parity_NONE;
    // UART_cfg.Stopbits = UART_STOPBIT_1;
    // Instead of doing this, the following function is used to configure UART_cfg
    UART_ConfigStructInit(&UART_cfg); // with the commented values.

    // This function initializes the peripheral and configures it with the struct's values
    UART_Init(LPC_UART0, &UART_cfg);

    //UARTFIFO_cfgt->FIFO_DMAMode = DISABLE;   // Use or not use DMA
    //UARTFIFO_cfg->FIFO_Level = UART_FIFO_TRGLEV0;   // An interrupt is triggered
    // or a DMA request is generated when one, two, four, eight, or 14 characters are received.
    //UARTFIFO_cfg->FIFO_ResetRxBuf = ENABLE;
    //UARTFIFO_cfg->FIFO_ResetTxBuf = ENABLE;
    // Instead of doing this, it's done similarly to UART_ConfigStructInit(&UART_cfg);
    UART_FIFOConfigStructInit(&UARTFIFO_cfg);
    //UARTFIFO_cfg.FIFO_Level = UART_FIFO_TRGLEV1;

    // Apply UART_FIFO configuration to LPC_UARTX (in this case UART0)
    UART_FIFOConfig(LPC_UART0, &UARTFIFO_cfg);

    // Enable transmission
    UART_TxCmd(LPC_UART0, ENABLE);

    // Enable Rx interrupt on UART0
    UART_IntConfig(LPC_UART0, UART_INTCFG_RBR, ENABLE);

    // Enable interrupt for Rx line state (in case of any type of error)
    UART_IntConfig(LPC_UART0, UART_INTCFG_RLS, ENABLE);

    // Enable interrupt for UART peripheral (every character due to the above configuration)
    NVIC_EnableIRQ(UART0_IRQn);

    return;
}